# Exercice autour des transactions

Dans un monde idéal il n'y aurait pas d'écritures en parallèle. Certains "systèmes de bases de données" règlent le problème de cette façon, en interdisant l'utilisation de la base par plusieurs utilisateurs (ou plusieurs connexions). Mais restons sérieux, dans un vrai système de gestion de base de données il peut y avoir des écritures en parallèle, plus précisément il peut y avoir des transactions en parallèle (avec des durées très variables).

## Pré-requis
Au sein de la base de données de votre choix. Une de test de préférence...

Créer trois tables respectant le schéma suivant:

![recipe-ingredient-mld.png](./recipe-ingredient-mld.png)

## Exercice 1 : Rédaction d'une transaction
- Réalisez une transaction qui execute tout ceci d'un coup:
	- Insère une recette "Salade de carottes"
	- Insère un ingrédient "Carotte"
	- Insère une association entre les deux avec la quantité de votre choix.

## Exercice 2: à dérouler étape par étape

Connectez-vous à mysql par la console. Ouvrez votre terminal ou votre `cmd` et connectez deux sessions via :  `mysql -u<user-name> -p`

- Au sein d'un premier client SQL :
	- Lisez le nombre d'enregistrements présents dans la table **Recipe_has_Ingredient**
	- Démarrer une transaction
	- Supprimer l'ensemble des enregistrements de la table **Recipe_has_Ingredient**
- Ouvrez une autre session client SQL
	- Lisez le nombre d'enregistrements présents dans la table **Recipe_has_Ingredient**
- Reprenez votre premier client SQL :
	- Réalisez un rollback
- Reprenez le second client SQL
	- Lisez le nombre d'enregistrements présents dans la table **Recipe_has_Ingredient**

